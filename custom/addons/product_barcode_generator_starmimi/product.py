# -*- coding: utf-8 -*-
#################################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013 Julius Network Solutions SARL <contact@julius.fr>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################

from openerp import fields, models
from openerp.osv import expression
from starmimi_barcode_generator import barcodegenerator


# product.template
class product_template(models.Model):
    _inherit = "product.template"

    serial_number = fields.Char("Serial", readonly=True, default="")

    @staticmethod
    def _string_digit_to_hex(s, padding=0):
        if padding == 0:
            padding = 1

        if (s is None) or (not isinstance(s, basestring)):
            return "0" * padding

        if s.isdigit() or s.isnumeric():
            i = int(s)
            result = "%X" % i
            return result if padding == 0 else result.rjust(padding, "0")

        return "0" * padding

    @staticmethod
    def _is_serial_number_invaild(serial):
        return (serial is None) or (not serial)

    def _get_next_sequence_id(self, cr, uid, product, context=None):
        padding = 5

        sequence_obj = self.pool.get('ir.sequence')
        if sequence_obj is None:
            return None

        if product.company_id and product.company_id.ean_sequence_id:
            nextid = sequence_obj.next_by_id(cr, uid, product.company_id.ean_sequence_id.id, context=context)
            h = self._string_digit_to_hex(nextid, padding=padding)
            # print("{0} > {1}".format(nextid, h))
            return h
        return None

    def update_serial_number(self, cr, uid, ids, context=None):
        # print(type(self), cr, uid, ids)
        product_temp_ids = self.browse(cr, uid, ids, context=context)
        for product_temp in product_temp_ids:
            # print("The parent product.template: {0}, Serial number is {1}".format(product_temp, product_temp.serial_number))
            if self._is_serial_number_invaild(product_temp.serial_number):
                ser = self._get_next_sequence_id(cr, uid, product_temp)
                product_temp.serial_number = ser

            product_temp.update_product_product_serial()
        return

    def update_product_product_serial(self):
        # print("update_product_product_serial", self.product_variant_count)
        # print(self.product_variant_ids)
        if self.product_variant_count <= 0:
            return
        for pp in self.product_variant_ids:
            pp.update_serial()
        return

    def get_serial_number(self):
        # print("The product.template's serial is {}".format(self.serial_number))
        return self.serial_number

    def name_search(self, cr, user, name, args=None, operator='ilike', context=None, limit=100):
        if not args:
            args = []
        if len(name) == 5:
            if operator in expression.NEGATIVE_TERM_OPERATORS:
                domain = [("serial_number", operator, name), ("name", operator, name)]
            else:
                domain = ["|", ("serial_number", operator, name), ("name", operator, name)]
            ids = self.search(cr, user, expression.AND([domain, args]), limit=limit, context=context)
        else:
            ids = self.search(cr, user, args, limit=limit, context=context)

        result = self.name_get(cr, user, ids, context=context)
        return result

    def create(self, cr, uid, vals, context=None):
        # print("product.template >>> before create", cr, uid, vals)
        res_id = super(product_template, self).create(cr, uid, vals, context=None)
        products = self.browse(cr, uid, [res_id])
        for p in products:
            # print(p)
            ser = self._get_next_sequence_id(cr, uid, p)
            p.serial_number = ser
            # print(p.serial_number)
            p.update_product_product_serial()
        return res_id


# product.product
class product_product(models.Model):
    _inherit = "product.product"

    serial_number = fields.Char("Serial", readonly=True, default="")
    default_code = fields.Char('Internal Reference', default="0000000000000")

    _barcodegen = barcodegenerator()

    def _get_starmimi_code_by_product(self, product):
        # print("product.product >>> _get_starmimi_code_by_product")

        serial = ""
        if not product:
            return serial

        # category
        codecategory = self._barcodegen.sequence_by_category(product.public_categ_ids)
        # print(codecategory)

        # serial number
        product.serial_number = product.product_tmpl_id.get_serial_number()
        # print(product.serial_number)

        # attribute
        codeattribute = "0" * 4
        if product.attribute_line_ids and product.attribute_value_ids:
            codeattribute = self._barcodegen.sequence_by_attribute(product.attribute_line_ids,
                                                                   product.attribute_value_ids)
        # print(codeattribute)

        # sequence of product
        sequenceproduct = "{category}{serialnum}{attribute}".format(
            category=codecategory,
            serialnum=product.serial_number,
            attribute=codeattribute).upper()
        # print(sequenceproduct)

        return sequenceproduct

    def generate_starmimi_code(self, cr, uid, ids, context=None):
        # print(type(self), cr, uid, ids)
        # print(type(ids))

        product_ids = self.browse(cr, uid, ids, context=context)
        for product in product_ids:
            # print("The parent product.template: {0}, Serial number is {1}".format(product.product_tmpl_id, product.product_tmpl_id.serial_number))
            seq = self._get_starmimi_code_by_product(product)
            self.write(cr, uid, [product.id], {'default_code': seq}, context=context)
            # print("write to {0}: default_code >>> {1}".format(product.id, seq))
            # print(seq)

        return True

    def update_serial(self):
        # print("product.product >>> update_serial")
        seq = self._get_starmimi_code_by_product(self)
        self.default_code = seq
        return

    def create(self, cr, uid, vals, context=None):
        # print("product.product >>> before create", cr, uid, vals)
        res_id = super(product_product, self).create(cr, uid, vals, context=None)
        products = self.browse(cr, uid, [res_id])
        # print(products)
        for p in products:
            # print(p)
            seq = self._get_starmimi_code_by_product(p)
            p.default_code = seq

        # print("after create")
        return res_id
