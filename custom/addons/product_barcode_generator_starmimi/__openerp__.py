# -*- coding: utf-8 -*-
{
    'name': 'Product barcode generator for starmimi',

    'summary': """
       """,

    'description': """
        """,

    'author': 'STARMIMI',
    'website': 'https://www.starmimi.com/',
    'category': 'Stock Management',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': [
        'base',
        'product',
        'website_sale',
        'product_barcode_generator',
        'product_attribute_starmimi',
    ],

    # always loaded
    'data': [
        'product_template_view.xml',
        'product_product_view.xml',
    ],

    # only loaded in demonstration mode
    'demo': [
    ],

    'installable': True,
    'active': False,
}