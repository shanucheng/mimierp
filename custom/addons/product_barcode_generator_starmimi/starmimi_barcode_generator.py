# -*- coding: utf-8 -*-
#################################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013 Julius Network Solutions SARL <contact@julius.fr>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################

from switch import Switch


class barcodegenerator:
    def __init__(self):
        return

    # Generator the sequence by product's attribute
    # ss is size
    # cc is color
    # return sscc
    def sequence_by_attribute(self, att_line_ids, attr_value_ids):
        if att_line_ids is None or attr_value_ids is None:
            return "0" * 4

        eansize = "0" * 2
        eancolor = "0" * 2

        for att in att_line_ids:
            attname = att.attribute_id.name.lower()
            for case in Switch(attname):
                if case("color", "顏色"):
                    for v in attr_value_ids:
                        if v.id in map(int, att.value_ids):
                            eancolor = self._attributevalue_to_seq(v)
                    break

                if case("size", "尺碼", "尺寸"):
                    for v in attr_value_ids:
                        if v.id in map(int, att.value_ids):
                            eansize = self._attributevalue_to_seq(v)
                    break

                if case():
                    # default, could also just omit condition or 'if True'
                    print("Warring!!! Get undefined attribute.")
                    break

        return eansize + eancolor

    # Generator the information of attribute by product's attribute
    # ss is size
    # cc is color
    # return sscc
    def _info_by_attribute(self, att_line_ids, attr_value_ids):
        if att_line_ids is None or attr_value_ids is None:
            return "-"

        infosize = ""
        infocolor = ""

        for att in att_line_ids:
            attname = att.attribute_id.name.lower()
            for case in Switch(attname):
                if case("color", "顏色"):
                    for v in attr_value_ids:
                        if v.id in map(int, att.value_ids):
                            infosize = self._attribute_info_to_seq(v)
                    break
                if case("size", "尺碼", "尺寸"):
                    for v in attr_value_ids:
                        if v.id in map(int, att.value_ids):
                            infocolor = self._attribute_info_to_seq(v)
                    break
                if case():
                    # default, could also just omit condition or 'if True'
                    print("Warring!!! Get undefined attribute.")
                    break

        return "{s}-{c}".format(s=infosize, c=infocolor)

    # Generator the sequence by product's category
    # aa is main category
    # bb is sub category
    # return aabb
    def sequence_by_category(self, categs):
        if categs is None:
            return "0" * 4
        l = len(categs)
        if l <= 0:
            return "0" * 4

        categ = categs[0]
        postfix = self._poscategory_to_seq(categ)
        prefix = self._poscategory_to_seq(categ.parent_id)
        return "{pre}{post}".format(pre=prefix, post=postfix)

    # Generator the information of attribute by product's variants
    @staticmethod
    def _attribute_info_to_seq(att_val):
        if att_val is None:
            return ""
        result = att_val.name
        return result

    # Generator the sequence of attribute by product's variants
    @staticmethod
    def _attributevalue_to_seq(att_val):
        if att_val is None:
            return "0" * 2
        result = att_val.code_id[0:2]
        return result.rjust(2, '0')

    # Generator the sequence by product's POS category
    @staticmethod
    def _poscategory_to_seq(categ):
        if categ is None:
            return "0" * 2
        return "{0:02d}".format(categ.sequence)
