###會員資料匯入###

* Name： 會員名稱
* Phone：會員電話
* Mobile：會員手機
* Street：odoo中的住址是由很幾個欄位組成，為了方便（他是國際地址格式）我只用Street這個欄位
* Birthdate：生日，這個欄位是String，是odoo內建的
* Level：會員等級
* Domain：會員分類
* Init Consumed Amount，這個是用來匯入的，然後加總的值會放到Consumed Amount
* Discountable：可折扣金額
* Discounted：以折扣金額
* Seven Day：7日鑑賞
* No take：未取
* Black List Date：黑名單日期

>匯入會員資料之前，請先把csv中的=以及"刪除，那個不符合odoo的格式
>另外，請先確認，對應的會員等級和會員分類已經被建立