from openerp import fields
from openerp.osv import osv


class res_partner(osv.Model):
    _inherit = 'res.partner'
    is_member = fields.Boolean('is_member')
    pos_order = fields.One2many('pos.order', 'partner_id')
    sale_order = fields.One2many('sale.order', 'partner_id')