# -*- coding: utf-8 -*-
{
    'name': "member_starmimi",

    'summary': """
        Member module for starmimi""",

    'description': """
        Member module for starmimi
    """,

    'author': "Jorden Chang",
    'website': "http://www.starmimi.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Sale',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'sale', 'point_of_sale'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'member_starmimi_view.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
    ],
}