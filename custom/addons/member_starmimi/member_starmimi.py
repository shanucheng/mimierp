# -*- coding: utf-8 -*-

from openerp import models, fields, api


class MemberLevel(models.Model):
    _name = "member_starmimi.level"
    name = fields.Char(required=True, select=True)


class MemberDomain(models.Model):
    _name = "member_starmimi.domain"
    name = fields.Char(required=True, select=True)


class MemberStarmimi(models.Model):
    _name = 'member_starmimi.member_starmimi'
    _inherits = {"res.partner": "partner_id"}

    """ This is included in res.partner
    member_name = fields.Char()
    telephone = fields.Char()
    mobile = fields.Char()
    email = fields.Char()
    address = fields.Char()
    birthdate = fields.Char()
    """
    partner_id = fields.Many2one("res.partner", string="Member Name", required=True, ondelete="cascade")
    customer = fields.Boolean('Customer', default=True)
    is_member = fields.Boolean('is_member', default=True)
    # The following is custom field.

    member_level = fields.Many2one('member_starmimi.level', ondelete='set null', string='Level')

    # Where did the customer register? E.g. official, store, or Yahoo!, etc..
    member_domain = fields.Many2one('member_starmimi.domain', ondelete='set null', string='Domain')

    """
     Indicates how much money have the customer consumed. In other word, this is sum of pos_order.amount_total,
     sale_order.amount_total, and init_consumed_amount.
     """
    consumed_amount = fields.Integer('Consumed Amount',compute='compute_total')

    discountable = fields.Integer()  # Indicates how much discount did the customer get.
    discounted = fields.Integer()  # Indicates how much discount have the customer used.
    seven_day = fields.Integer()  # Indicates how many time did the customer return the goods during 7 days.
    no_take = fields.Integer()  # Indicates how many time did the customer not take goods.
    black_list_date = fields.Date()  # Not used now.

    """
    The following field is for importing data. For example, you import a new customer record with non zero
    consumed_amount, assign it to init_consumed_amount instead of consumed_amount. Don't fill in this field directly.
    """
    init_consumed_amount = fields.Integer()

    @api.depends('pos_order.amount_total', 'sale_order.amount_total', 'init_consumed_amount')
    def compute_total(self):
        # Computes sum of total of pos order and sale order
        total = self.init_consumed_amount
        total = total + sum(order.amount_total for order in self.pos_order)
        total = total + sum(order.amount_total for order in self.sale_order)
        print total
        self.consumed_amount = total
