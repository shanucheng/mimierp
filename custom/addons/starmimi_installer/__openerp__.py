# -*- coding: utf-8 -*-
{
    'name': "Starmimi installer",

    'summary': """
    """,

    'description': """
    """,

    'author': "STARMIMI",
    'website': "http://www.starmimi.com",

    'category': 'Starmimi Management',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': [
        'base',
        'account_accountant',
        'product',
        'stock',
        'sale',
        'point_of_sale',
        'purchase',
        'website',
        'website_sale',

        'product_barcode_generator',
        'pos_keyboard',
        'pos_product_available',

        'product_attribute_starmimi',
        'product_barcode_generator_starmimi',
        'product_list_view_by_web_category_starmimi',
        'sample_plate_starmimi',
        'member_starmimi',
        'presco',
        'usermenu',
        'product_tag_starmimi'
    ],

    'installable': True,
    'active': False,
}
