# -*- coding: utf-8 -*-
#################################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013 Julius Network Solutions SARL <contact@julius.fr>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################

from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.exceptions import Warning


# Attribute value #
class product_attribute_value(models.Model):
    _inherit = "product.attribute.value"

    code_id = fields.Char("Code", required=True, default='0',
                          help="Gives the code id when displaying a list of attribute.")

    @staticmethod
    def check_code_id(product_attribute, code_id, pavid=None):
        if product_attribute is None:
            return False
        if code_id is None:
            return False

        # print(product_attribute.value_ids)
        code_ids = []
        for obj in product_attribute.value_ids:
            if pavid is not None and obj.id == pavid:
                continue
            code_ids += obj.code_id
        # print("Values id: {}".format(code_ids))
        if code_id in code_ids:
            return False

        return True

    def create(self, cr, uid, vals, context=None):
        # print(self, vals)
        key_attid = "attribute_id"
        key_codeid = "code_id"
        if key_attid in vals and key_codeid in vals:
            attid = vals[key_attid]
            newcodeid = vals[key_codeid]
            result = False
            with api.Environment.manage():
                env = api.Environment(cr, attid, {})
                pdtatt = env['product.attribute'].browse(attid)
                # print(type(pdtatt), pdtatt)
                result = self.check_code_id(pdtatt, newcodeid)
            if not result:
                raise Warning(_("The code is existed."))

        new_id = super(product_attribute_value, self).create(cr, uid, vals, context=context)
        return new_id

    def write(self, cr, uid, ids, vals, context=None):
        # print(self, cr, uid, ids, vals)

        # check code_id is already exists
        key = "code_id"
        if key in vals:
            newcodeid = vals[key]
            # print(newcodeid)
            with api.Environment.manage():
                env = api.Environment(cr, uid, {})
                for theid in ids:
                    pav = env["product.attribute.value"].browse(theid)
                    if pav is None:
                        continue
                    # print(pav, theid, pav.attribute_id)
                    pdtatt = env["product.attribute"].browse(pav.attribute_id.id)
                    # print(type(pdtatt), pdtatt)
                    result = self.check_code_id(pdtatt, newcodeid, theid)
                    if not result:
                        raise Warning(_("The code is existed."))

        res = super(product_attribute_value, self).write(cr, uid, ids, vals, context=context)

        # if update name
        key = "name"
        if key in vals:
            with api.Environment.manage():
                env = api.Environment(cr, uid, {})
                for theid in ids:
                    pav = env["product.attribute.value"].browse(theid)
                    if pav is None:
                        continue
                    # print(pav.product_ids)
                    # if update name, need to update product.product
                    for product in pav.product_ids:
                        product.update_color()
                        product.update_size()
        return res
