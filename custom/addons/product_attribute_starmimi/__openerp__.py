# -*- coding: utf-8 -*-
{
    'name': 'Product attribute for starmimi',

    'summary': """
       """,

    'description': """
        """,

    'author': 'STARMIMI',
    'website': 'https://www.starmimi.com/',
    'category': 'Product',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': [
        'base',
        'product'
    ],

    # always loaded
    'data': [
        'attribute_value_view.xml',
    ],

    # only loaded in demonstration mode
    'demo': [
    ],

    'installable': True,
    'active': False,
}
