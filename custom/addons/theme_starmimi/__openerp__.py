{
  'name':'Starmimi theme',
  'description': 'Fashion e-Commerce of Starmimi inc.',
  'version':'1.0',
  'author':'starmimi.it',

  'data': [
    'views/layout.xml',
    'views/pages.xml',
    'views/assets.xml',
    'views/snippets.xml',
    'views/options.xml'
  ],
  'category': 'Theme/Creative',
  'depends': ['website', 'website_less', 'sale'],
}