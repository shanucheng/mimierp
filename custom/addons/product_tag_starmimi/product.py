# -*- coding: utf-8 -*-

from openerp import models, fields, api


class product_tag(models.Model):
    _name = "product.tag"

    name = fields.Char("Tag name", required=True)


class product(models.Model):
    _inherit = "product.template"
    other_tags = fields.Many2many("product.tag", "product_to_tag", "prd_id", "tag_id", string="Tags")
