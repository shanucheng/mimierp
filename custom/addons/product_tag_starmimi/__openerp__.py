# -*- coding: utf-8 -*-
{
    'name': "Product's tag",

    'summary': """
        Can add many tags on product
    """,

    'description': """
    """,

    'author': "Starmimi",
    'website': "",

    'category': 'Product',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': [
        'base',
        'product',
    ],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'product_view.xml',
    ],

    # only loaded in demonstration mode
    'demo': [
    ],
}