# -*- coding: utf-8 -*-
{
    'name': "User menu override",

    'summary': """
    """,

    'description': """
    """,

    'author': "STARMIMI",
    'website': "http://www.starmimi.com",

    'category': 'Starmimi',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': [
        'base',
    ],

    # always loaded
    'data': [
        'usermenu_view.xml',
    ],
    'qweb': [
        'static/src/xml/usermenu_override.xml'
    ],

    'installable': True,
    'active': False,
}