# -*- coding: utf-8 -*-
{
    'name': "Presco",
    'author': "Starmimi",
    'category': 'Warehouse Management',
    'version': '0.1',
    'depends': [
        'report',
        'stock',
    ],
    'data': [
        'presco_report.xml',
        'views/report_prescowaybill.xml',
        'static/less/style.less',
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}