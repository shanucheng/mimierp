# -*- coding: utf-8 -*-
#################################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013 Julius Network Solutions SARL <contact@julius.fr>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################

from openerp import fields, models, api
from openerp.tools.translate import _
from switch import Switch


# class Starmimi_Product_Product
class Starmimi_Product_Product(models.Model):
    _inherit = 'product.product'

    attribute_color = fields.Char("Color")
    attribute_size = fields.Char("Size")

    # Attribute Color
    def update_color(self):
        # print("update_color")
        self.attribute_color = ""
        if self.attribute_line_ids is None or self.attribute_value_ids is None:
            return

        # print("{}".format(self.attribute_line_ids))
        for att in self.attribute_line_ids:
            attname = att.attribute_id.name.lower()
            # print("{}".format(attname))
            for case in Switch(attname):
                if case("color", "顏色"):
                    for v in self.attribute_value_ids:
                        # print("{}".format(v))
                        if v.id in map(int, att.value_ids):
                            # print("{}".format(v.name))
                            self.attribute_color = v.name
                            return
                    break
                if case():
                    # default, could also just omit condition or 'if True'
                    break
        return

    # Attribute Size
    def update_size(self):
        # print("update_size")
        self.attribute_size = ""
        if self.attribute_line_ids is None or self.attribute_value_ids is None:
            return

        # print("{}".format(self.attribute_line_ids))
        for att in self.attribute_line_ids:
            attname = att.attribute_id.name.lower()
            # print("{}".format(attname))
            for case in Switch(attname):
                if case("size", "尺碼", "尺寸"):
                    for v in self.attribute_value_ids:
                        # print("{}".format(v))
                        if v.id in map(int, att.value_ids):
                            # print("{}".format(v.name))
                            self.attribute_size = v.name
                            return
                    break
                if case():
                    # default, could also just omit condition or 'if True'
                    break
        return

    def create(self, cr, uid, vals, context=None):
        # print("before create >> {}".format(type(self)))
        res_id = super(Starmimi_Product_Product, self).create(cr, uid, vals, context=None)
        products = self.browse(cr, uid, [res_id])
        # print(products)
        for p in products:
            print(p)
            p.update_color()
            p.update_size()

        # print("after create >> {}".format(type(self)))
        return res_id

