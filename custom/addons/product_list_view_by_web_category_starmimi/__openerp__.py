# -*- coding: utf-8 -*-
{
    'name': "Product list view by web category",

    'summary': """
    """,

    'description': """
    """,

    'author': "STARMIMI",
    'website': "http://www.starmimi.com",

    'category': 'Starmimi Management',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': [
        'base',
        'product',
        'website',
        'website_sale',
    ],

    'data': [
        'product_product_view.xml',
        'product_category_report.xml',
        'report/report_product.xml'
    ],

    'installable': True,
    'active': False,
}
