# -*- coding: utf-8 -*-
#################################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013 Julius Network Solutions SARL <contact@julius.fr>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################

from openerp import fields, models, api
from openerp.osv import expression
from openerp.tools.translate import _
from openerp.exceptions import Warning


# region Material
class material_template(models.Model):

    def _get_name(self):
        cat = self
        n = cat.reference_number
        if cat.supplier_id:
            n = cat.supplier_id.name + "-" + cat.reference_number
        return n

    @api.depends('reference_number', 'supplier_id')
    def _get_complete_name(self):
        for rec in self:
            result = rec._get_name()
            rec.complete_name = result

    @api.multi
    def name_get(self):
        return [(cat.id, cat._get_name()) for cat in self]

    _name = "material.template"

    name = fields.Char("Name")
    complete_name = fields.Char("Name", compute="_get_complete_name")
    reference_number = fields.Char("Reference number", required=True)
    image = fields.Binary("Image")
    description = fields.Text("Description")
    supplier_id = fields.Many2one("res.partner", string="Supplier")
# endregion


# region Material tag
class material_tag(models.Model):
    _name = "material.tag"

    name = fields.Char("Name", required=True)
# endregion


# region Material category
class material_category(models.Model):
    _name = "material.category"

    def _get_name(self):
        cat = self
        n = cat.name
        if cat.is_only_or_not:
            n = "(*)"+n
        return n

    @api.depends('name')
    def _get_complete_name(self):
        for rec in self:
            result = rec._get_name()
            rec.complete_name = result

    @api.multi
    def name_get(self):
        return [(cat.id, cat._get_name()) for cat in self]

    def write(self, cr, uid, ids, vals, context=None):
        print("material_category write>>>", cr, uid, ids, vals)
        # check the category is invalid
        k = "is_only_or_not"
        if k in vals and ids is not None:
            is_true = vals[k]
            print(is_true)
            if is_true:
                mls = self.pool.get("material.line").search(cr, uid, [("category_id", "=", ids[0])])
                # print(mls, len(mls))
                samples = self.pool.get("sample.plate").search(cr, uid, [("material_line_ids", "in", mls)])
                # print(samples, len(samples))
                if len(mls) != len(samples):
                    raise Warning(_("You can not change this category to unique! "
                                    "There are one more of samples to use it."))
        return super(material_category, self).write(cr, uid, ids, vals, context=context)

    name = fields.Char("Name", required=True)
    complete_name = fields.Char("Name", compute="_get_complete_name")
    is_only_or_not = fields.Boolean("Is only or not", default=False)
    material_line_ids = fields.One2many("material.line", inverse_name="category_id", string="Material line ids")
# endregion


# region Material line
class material_line(models.Model):
    def _check_category(self, cr, uid, ids=None, cate_id=0, sample_id=0):
        # print(self, cr, uid, ids, cate_id, sample_id)
        mat_cate_obj = self.pool.get("material.category")
        mat_cate_id = mat_cate_obj.search(cr, uid, [("id", "=", cate_id), ("is_only_or_not", "=", "TRUE")])
        # print(mat_cate_id)
        if not mat_cate_id:
            return True

        domain = [("category_id", "=", cate_id)]
        domain_sample = ("sample_plate_id", "=", sample_id)
        if sample_id != 0:
            domain.append(domain_sample)
        # print(domain)
        mat_line_ids = self.search(cr, uid, domain)
        # print(mat_line_ids)
        if mat_line_ids:
            return False
        return True

    def create(self, cr, uid, vals, context=None):
        # print("material_line create>>>", cr, uid, vals)
        key_cate = "category_id"
        key_sample = "sample_plate_id"
        if key_cate in vals and key_sample in vals:
            cate_id = vals[key_cate]
            sample_id = vals[key_sample]
            if not self._check_category(cr, uid, cate_id=cate_id, sample_id=sample_id):
                raise Warning(_("You can not setting same category of material, "
                                "if the category of materials is unique."))
        return super(material_line, self).create(cr, uid, vals, context=context)

    def write(self, cr, uid, ids, vals, context=None):
        # print("material_line write>>>", cr, uid, ids, vals)
        key_cate = "category_id"
        if key_cate in vals and ids is not None:
            cate_id = vals[key_cate]
            if not self._check_category(cr, uid, ids=ids, cate_id=cate_id):
                raise Warning(_("You can not setting same category of material, "
                                "if the category of materials is unique."))
        return super(material_line, self).write(cr, uid, ids, vals, context=context)

    _name = "material.line"

    sample_plate_id = fields.Many2one("sample.plate", string="Sample plate", required=True, ondelete='cascade')
    category_id = fields.Many2one("material.category", string="Category", required=True, ondelete='cascade')
    material_id = fields.Many2one("material.template", string="Material", required=True, ondelete='cascade')
    tag_ids = fields.Many2many("material.tag", "mat_id", "tag_id", string="Tags")
# endregion


# region Sample plate image
class sample_plate_image(models.Model):
    @api.one
    def _get_sample_plate_ids(self):
        if not self.id:
            return

        self._cr.execute('SELECT sample_id '
                         'FROM sample_plate_image_ids where image_id=%s',
                         (self.id,))
        ids = self._cr.fetchall()
        if ids is None:
            self.sample_plate_ids = None
            return

        samples = self.env['sample.plate']
        for obj_id in ids:
            result = self.pool.get("sample.plate").browse(self._cr, self._uid, obj_id)
            if result is None:
                continue
            samples += result[0]
        self.sample_plate_ids = samples

    _name = "sample.plate.image"
    name = fields.Char("Name", required=True)
    description = fields.Text("Description")
    image = fields.Binary("Image")
    sample_plate_ids = fields.Many2many("sample.plate", string="Samples",
                                        compute="_get_sample_plate_ids")
# endregion


# region Sample plate category
class sample_plate_category(models.Model):
    # Note:
    # if you are overriding name_get method and value is coming based on context key then I would prefer [always_reload]
    # option should set on that [Many2one] field, so I would write something like below.
    # ex.
    # <field name="parent_id" context="{'partner_category_display':'short'}" options='{"always_reload": True}'/>
    def _get_name(self):
        res = []
        cat = self
        while cat:
            res.append(cat.name)
            cat = cat.parent_id
        return res

    @api.multi
    def name_get(self):
        return [(cat.id, " / ".join(reversed(cat._get_name()))) for cat in self]

    @api.depends('name', 'parent_id')
    def _get_complete_name(self):
        for rec in self:
            result = " / ".join(reversed(rec._get_name()))
            rec.complete_name = result

    name = fields.Char("Name", required=True, seletc=True)
    complete_name = fields.Char("Name", compute="_get_complete_name")
    parent_id = fields.Many2one("sample.plate.category", string="Parent Category", select=True, ondelete='cascade')
    child_id = fields.One2many("sample.plate.category", "parent_id", string="Children Categories")
    sequence = fields.Integer('Sequence', select=True)
    parent_left = fields.Integer('Left Parent', select=1)
    parent_right = fields.Integer('Right Parent', select=1)

    _name = "sample.plate.category"
    _description = "Category of sample plate"
    _parent_name = "parent_id"
    _parent_store = True
    _parent_order = 'sequence, name'
    _order = 'parent_left'
    _constraints = [
        (models.Model._check_recursion, 'Error ! You cannot create recursive categories.', ['parent_id'])
    ]
# endregion


# region Sample plate note
class sample_plate_note(models.Model):
    _name = "sample.plate.note"
    _defaults = {
        'user_id': lambda self, cr, uid, context: uid,
    }

    description = fields.Text("Description", required=True)
    datetime = fields.Datetime("Datetime", index=True, default=fields.datetime.now())
    sample_plate_id = fields.Many2one("sample.plate", string="Sample plate")
    user_id = fields.Many2one("res.users", string="Created by", help="The user responsible for this note")
    is_print = fields.Boolean("Is Print?", default=True)
# endregion


# region Sample plate
class sample_plate(models.Model):
    def _get_default_currency_price(self, cr, uid, context=None):
        res = self.pool.get("res.currency").search(cr, uid, [("id", "=", "CNY")], context=context)
        return res and res[0] or False

    def _check_serial(self, cr, uid, vals):
        key = "serial"
        if key in vals:
            exists_data_id = self.search(cr, uid, [(key, "=", vals[key])])
            if exists_data_id:
                return False

        return True

    def name_search(self, cr, user, name, args=None, operator='ilike', context=None, limit=100):
        if not args:
            args = []
        if operator in expression.NEGATIVE_TERM_OPERATORS:
            domain = [("serial", operator, name), ("name", operator, name)]
        else:
            domain = ["|", ("serial", operator, name), ("name", operator, name)]
        ids = self.search(cr, user, expression.AND([domain, args]), limit=limit, context=context)
        return self.name_get(cr, user, ids, context=context)

    def create(self, cr, uid, vals, context=None):
        if context is None:
            context = {}

        if not self._check_serial(cr, uid, vals):
            raise Warning(_("The Serial is existed."))

        return super(sample_plate, self).create(cr, uid, vals, context=context)

    def write(self, cr, uid, ids, vals, context=None):
        if not self._check_serial(cr, uid, vals):
            raise Warning(_("The Serial is existed."))

        res = super(sample_plate, self).write(cr, uid, ids, vals, context=context)
        return res

    _name = "sample.plate"

    name = fields.Char("Name")
    serial = fields.Char("Serial", required=True, index=True, copy=False)
    image = fields.Binary("Image")
    category = fields.Many2one("sample.plate.category", string="Category", required=True)
    description = fields.Text("Description")
    release_date = fields.Date("Release date", index=True, copy=False)

    product_template_id = fields.Many2one("product.template", string="Product template")
    material_line_ids = fields.One2many("material.line", "sample_plate_id", string="Textiles")
    images = fields.Many2many("sample.plate.image",
                              "sample_plate_image_ids", "sample_id", "image_id",
                              string="Images", copy=False)
    notes = fields.One2many("sample.plate.note", "sample_plate_id", string="Notes")

    designer_ids = fields.Many2many("res.users", "sample_plate_designer", "sample_id", "user_id", string="Designer")
    supplier_id = fields.Many2one("res.partner", string="Supplier")
    cloth_of_quantity = fields.Char("Quantity")

    defaults = {
        "currency_id_price": _get_default_currency_price,
    }
# endregion



