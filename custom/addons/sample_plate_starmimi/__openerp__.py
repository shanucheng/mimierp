# -*- coding: utf-8 -*-
{
    'name': 'Sample plate for starmimi',

    'summary': """
       """,

    'description': """
        """,

    'author': 'STARMIMI',
    'website': 'https://www.starmimi.com/',
    'category': 'Product',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': [
        'base',
        'product',

        'web_tree_image',
    ],

    # always loaded
    'data': [
        'security/security.xml',
        'sample_plate_view.xml',
        'security/ir.model.access.csv',
        'product_view.xml',
        'sample_plate_report.xml',
        'views/report_sample_plate.xml',
    ],

    # only loaded in demonstration mode
    'demo': [
    ],

    'installable': True,
    'active': False,
}
