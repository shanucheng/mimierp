# -*- coding: utf-8 -*-
{
    'name': "pos_client_vat",

    'summary': """
        Textbox for typing vat of client in PoS""",

    'description': """
        Textbox for typing vat of client in PoS
    """,

    'author': "Jorden Chang",
    'website': "http://www.starmimi.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Point Of Sale',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['point_of_sale'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'templates.xml',
    ],
    'qweb': [
        'static/src/xml/pos.xml',
    ],
    'installable':True,
}