function pos_client_vat(instance, module){
    var OrderSuper = module.Order;
    module.Order = module.Order.extend({
        saveVat: function() {
            this.client_vat = $('.vat-input').val();
        },
        getVat: function() {
            return this.client_vat;
        },
        addPaymentline: function(cashregister) {
            this.saveVat();
            OrderSuper.prototype.addPaymentline.call(this, cashregister);
        }
    });
}

(function(){
    var _super = window.openerp.point_of_sale;
    window.openerp.point_of_sale = function(instance){
        _super(instance);
        var module = instance.point_of_sale;

        pos_client_vat(instance, module);

        $('<link rel="stylesheet" href="/pos_client_vat/static/src/css/pos.css"/>').appendTo($("head"))
    }
})()